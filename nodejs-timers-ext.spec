%{?nodejs_find_provides_and_requires}
%global packagename timers-ext
%global enable_tests 0
Name:		nodejs-timers-ext
Version:	0.1.0
Release:	2
Summary:	Timers extensions
License:	MIT
URL:		https://github.com/medikoo/timers-ext
Source0:	https://registry.npmjs.org/%{packagename}/-/%{packagename}-%{version}.tgz
BuildArch:	noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       	nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       	npm(tad)
%endif
%description
Timers extensions

%prep
%setup -q -n package
%nodejs_fixdep next-tick ^1.1.0

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/%{packagename}
cp -pr package.json *.js \
	%{buildroot}%{nodejs_sitelib}/%{packagename}
%nodejs_symlink_deps

%check
%nodejs_symlink_deps --check
%if 0%{?enable_tests}
%{__nodejs} -e 'require("./")'
%{__nodejs} %{nodejs_sitelib}/tad/bin/tad
%endif

%files
%{!?_licensedir:%global license %doc}
%doc *.md CHANGES
%license LICENCE
%{nodejs_sitelib}/%{packagename}

%changelog
* Sat Jul 02 2022 wangkai <wangkai385@h-partners.com> - 0.1.0-2
- Modify require version to fix unresolvable

* Fri Aug 21 2020 wangyue <wangyue92@huawei.com> - 0.1.0-1
- package init
